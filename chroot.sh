#!/usr/bin/env bash
set -e
source install.conf
source utils.sh

#######################################
# Sets the systems time, date and locale.
# Globals:
#   TIMEZONE
#   LOCALE
#   KEYMAP
# Arguments:
#   None
#######################################
function configure_time_date_locale() {
    log_info "Configuring timezone, date and locale"
    timedatectl set-ntp true
    timedatectl set-timezone "$TIMEZONE"
    # ln -sf "/usr/share/zoneinfo/$TIMEZONE" /etc/localtime
    echo "$LOCALE $(echo $LOCALE | cut -d '.' -f 2)" >>/etc/locale.gen
    locale-gen
    echo "LANG=\"$LOCALE\"" >>/etc/locale.conf
    echo "KEYMAP=\"${KEYMAP}\"" >/etc/vconsole.conf
}

#######################################
# Generates the system's /etc/hostname and /etc/hosts file.
# Globals:
#   None
# Arguments:
#   hostname - string
#######################################
function configure_hosts() {
    log_info "Configuring system hostname"
    echo "$1" >/etc/hostname
    echo "
    127.0.0.1     localhost.localdomain localhost
    ::1           localhost.localdomain localhost
    127.0.1.1     $1.localdomain $1
    " >>/etc/hosts
}

#######################################
# Enables systemd services
# Globals:
#   VIRTUALIZATION_TOOLS_ENABLE
#   FIREWALL_ENABLE
#   POWERSAVING_ENABLE
# Arguments:
#   None
#######################################
function enable_services() {
    systemctl enable NetworkManager fstrim.timer

    if [[ $VIRTUALIZATION_TOOLS_ENABLE =~ ^[Yy]$ ]]; then
        systemctl enable dnsmasq ebtables libvirtd
    fi

    if [[ $FIREWALL_ENABLE =~ ^[Yy]$ ]]; then
        systemctl enable firewalld
    fi

    if [[ $POWERSAVING_ENABLE =~ ^[Yy]$ ]]; then
        systemctl enable tlp powertop
    fi

    if [[ $GNOME_ENABLE =~ ^[Yy]$ ]]; then
        systemctl enable gdm
    fi
}

#######################################
# Installs and configures the GRUB bootloader
# Globals:
#   None
# Arguments:
#   crypt partition - device path
#######################################
function configure_bootloader() {
    local crypt_part="$1"
    local crypt_name=$(format_crypt_name $crypt_part)
    local crypt_path="/dev/mapper/$crypt_name"
    local grub_crypt_entry="rd.luks.name=$(blkid -s UUID -o value $crypt_part)=${crypt_name} root=${crypt_path}"
    local grub_cmdline_entries="apparmor=1 security=apparmor loglevel=3 quiet $grub_crypt_entry"

    log_info "Installing the GRUB bootloader"
    grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=archlinux
    sed -i "s/GRUB_CMDLINE_LINUX_DEFAULT/#GRUB_CMDLINE_LINUX_DEFAULT/" /etc/default/grub
    echo "GRUB_CMDLINE_LINUX_DEFAULT=\"${grub_cmdline_entries}\"" >>/etc/default/grub
    grub-mkconfig -o /boot/grub/grub.cfg
}

#######################################
# Configures the system's initramfs
# Globals:
#   None
# Arguments:
#   None
#######################################
function configure_initramfs() {
    log_info "Configuring initramfs"
    mv /etc/mkinitcpio.conf /etc/mkinitcpio.conf.bak
    echo 'MODULES=""' >>/etc/mkinitcpio.conf
    echo 'BINARIES=""' >>/etc/mkinitcpio.conf
    echo 'FILES=""' >>/etc/mkinitcpio.conf
    echo 'HOOKS="base systemd autodetect keyboard sd-vconsole modconf block btrfs sd-encrypt filesystems fsck"' >>/etc/mkinitcpio.conf

    if [[ $LINUX_ZEN_ENABLE =~ ^[Yy]$ ]]; then
        mkinitcpio -p linux-zen
    elif [[ $LINUX_LTS_ENABLE =~ ^[Yy]$ ]]; then
        mkinitcpio -p linux-lts
    else
        mkinitcpio -p linux
    fi
}

#######################################
# Creates additional system users
# Globals:
#   None
# Arguments:
#   None
#######################################
function add_users() {
    log_info "Creating system users"

    local add_new="Y"
    while [[ $add_new =~ ^[Yy]$ ]]; do
        local username=""
        local add_to_wheel="N"

        read -p "Enter username: " username
        useradd -m $username
        passwd $username

        read -p "Add user to the 'wheel' group? [y/N]: " add_to_wheel
        if [[ $add_to_wheel =~ ^[Yy]$ ]]; then
            usermod -aG wheel $username
        fi

        read -p "Create additional users? [Y/n]: " add_new
    done
}

function main() {
    local partlist=("$TARGET_DISK"*)

    configure_time_date_locale
    configure_hosts "$HOSTNAME"
    enable_services
    configure_bootloader ${partlist[3]}
    configure_initramfs
    log_info "Setting root password"
    passwd root
    sed -i 's/^#\s*\(%wheel\s\+ALL=(ALL)\s\+NOPASSWD:\s\+ALL\)/\1/' /etc/sudoers
    add_users
}

main
